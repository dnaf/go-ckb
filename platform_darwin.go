package ckb

const strConnected = "/var/run/ckb0/connected"
const strDevPrefix = "/var/run/ckb"
const fmtDev = "/var/run/ckb%[1]d"
const fmtDevCmd = "/var/run/ckb%[1]d/cmd"
