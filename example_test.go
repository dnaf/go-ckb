package ckb

import (
	"fmt"
	"image/color"
)

func Example() {
	// list devices
	devices, err := Devices()
	if err != nil {
		panic(err)
	}

	// open first device
	dev, err := Open(devices[0].Index)
	if err != nil {
		panic(err)
	}

	// use independant notification channel so we don't interfere with other applications using this device
	err = dev.SetNotificationChannel(3, true)
	if err != nil {
		panic(err)
	}

	// erase the state of the third mode
	err = dev.EraseMode(3)
	if err != nil {
		panic(err)
	}
	// and switch to it
	err = dev.SwitchMode(3)
	if err != nil {
		panic(err)
	}

	// make every LED blue
	err = dev.SetAllLEDs(3, &color.RGBA{0, 0, 255, 255})
	if err != nil {
		panic(err)
	}

	for {
		// poll for events
		ev, err := dev.Poll()
		if err != nil {
			panic(err)
		}
		// handle event
		switch ev := ev.(type) {
		case *KeyEvent:
			if ev.Pressed {
				fmt.Printf("%s pressed\n", ev.Key)
			} else {
				fmt.Printf("%s released\n", ev.Key)
			}
		}
	}
}
