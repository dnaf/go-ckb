package ckb

import (
	"errors"
	"fmt"
	"strings"
)

var ErrDeviceNotFound = errors.New("ckb: device not found")

type DeviceError struct {
	Index DeviceIndex
	Err   error
}

func deviceError(i DeviceIndex, err error) error {
	return &DeviceError{Index: i, Err: err}
}
func (e *DeviceError) Error() string {
	return fmt.Sprintf("ckb (ckb%d): %v", e.Index, e.Err)
}
func (e *DeviceError) Unwrap() error {
	return e.Err
}

type MultiError struct {
	Errs []error
}

func multi(errs ...error) *MultiError {
	multi := &MultiError{}
	for _, e := range errs {
		if e != nil {
			multi.Errs = append(multi.Errs, e)
		}
	}
	if len(multi.Errs) == 0 {
		return nil
	}
	return multi
}
func (e *MultiError) Error() string {
	errstrs := make([]string, len(e.Errs))
	for i := range e.Errs {
		errstrs[i] = e.Errs[i].Error()
	}
	return fmt.Sprintf("multierror: %s", strings.Join(errstrs, "; "))
}
