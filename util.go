package ckb

import (
	"fmt"
	"image/color"
)

func colToHex(col color.Color) string {
	r, g, b, _ := col.RGBA()
	i := uint64(r&0xff00)<<8 +
		uint64(g&0xff00) +
		uint64(b&0xff00)>>8
	return fmt.Sprintf("%06x", i)
}
