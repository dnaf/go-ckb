package ckb

const strConnected = "/dev/input/ckb0/connected"
const strDevPrefix = "/dev/input/ckb"
const fmtDev = "/dev/input/ckb%[1]d"
const fmtDevCmd = "/dev/input/ckb%[1]d/cmd"
const fmtDevNotify = "/dev/input/ckb%[1]d/notify%[2]d"
