package main

import (
	"fmt"
	"gitlab.com/dnaf/go-ckb"
	"image/color"
	"log"
	"os"
	"strings"
)

func main() {
	log.SetFlags(0)

	if len(os.Args) != 2 {
		log.Fatal("usage: typelighting SUBSTR")
	}
	match := strings.ToLower(os.Args[1])

	devices, err := ckb.Devices()
	if err != nil {
		panic(err)
	}

	// list all devices
	log.Println("available devices:")
	for _, dev := range devices {
		log.Printf("- %s (%s)", dev.Description, dev.SerialNumber)
	}
	log.Println()

	// find device matching substring
	var devInfo *ckb.DeviceInfo
	for _, d := range devices {
		desc := strings.ToLower(d.Description)
		serial := strings.ToLower(d.SerialNumber)
		if strings.Contains(desc, match) || strings.Contains(serial, match) {
			devInfo = &d
			break
		}
	}
	if devInfo == nil {
		panic(fmt.Sprintf("no devices matching %q were found\n", os.Args[1]))
	}
	log.Printf("using %s (%s)\n", devInfo.Description, devInfo.SerialNumber)

	// open device
	dev, err := ckb.Open(devInfo.Index)
	if err != nil {
		panic(err)
	}
	defer dev.Close()

	// use an independant notification channel
	// not necessary, but good practice
	if err := dev.SetNotificationChannel(3, true); err != nil {
		panic(err)
	}

	prevMode, err := dev.GetMode()
	if err != nil {
		panic(err)
	}
	log.Printf("current mode: %d", prevMode)
	defer func() {
		log.Printf("switching back to mode %d\n", prevMode)
		dev.SwitchMode(prevMode)
	}()

	mode := 4 // you can use any mode from 1-6; see ckb-daemon's docs for more information
	log.Printf("switching to mode %d", mode)

	// erase existing settings and switch to mode
	if err := dev.EraseMode(mode); err != nil {
		panic(err)
	}
	if err := dev.SwitchMode(mode); err != nil {
		panic(err)
	}

	log.Println("making it blue")
	if err := dev.SetAllLEDs(mode, &color.RGBA{0, 0, 16, 255}); err != nil {
		panic(err)
	}

	if err := dev.NotifyAllKeys(); err != nil {
		panic(err)
	}

	for {
		ev, err := dev.Poll()
		if err != nil {
			panic(err)
		}

		switch ev := ev.(type) {
		case *ckb.KeyEvent:
			col := color.RGBA{0, 0, 16, 255}
			if ev.Pressed {
				col = color.RGBA{127, 127, 255, 255}
			}
			if err := dev.SetLED(mode, ev.Key, &col); err != nil {
				panic(err)
			}
		}
	}
}
