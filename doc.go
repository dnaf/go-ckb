// Package ckb provides an interface to ckb-daemon, a daemon for communicating with Corsair peripherals.
//
// See https://github.com/ckb-next/ckb-next
package ckb
