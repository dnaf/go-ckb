// +build debug

package ckb

import (
	"fmt"
	"os"
)

func debugf(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, format, args...)
}
