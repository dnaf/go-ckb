package ckb

import (
	"bufio"
	"errors"
	"fmt"
	"image/color"
	"os"
	"strconv"
	"strings"
	"time"
)

type DeviceIndex int

// DeviceInfo represents information about a connected device.
type DeviceInfo struct {
	Index        DeviceIndex
	Path         string
	SerialNumber string
	Description  string
}

// Devices returns a list of connected devices.
func Devices() ([]DeviceInfo, error) {
	file, err := os.Open(strConnected)
	if err != nil {
		return []DeviceInfo{}, fmt.Errorf("ckb: open device list: %w", err)
	}
	defer file.Close()

	devices := make([]DeviceInfo, 0, 9)
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		info := strings.SplitN(scanner.Text(), " ", 3)
		if len(info) == 1 && len(info[0]) == 0 {
			continue
		}
		if len(info) != 3 {
			return []DeviceInfo{}, fmt.Errorf("ckb: parse device list: invalid line %q", scanner.Text())
		}
		i, err := strconv.Atoi(strings.TrimPrefix(info[0], strDevPrefix))
		if err != nil {
			return []DeviceInfo{}, fmt.Errorf("ckb: parse device list: %w", err)
		}
		devices = append(devices, DeviceInfo{
			Index:        DeviceIndex(i),
			Path:         info[0],
			SerialNumber: info[1],
			Description:  info[2],
		})
	}
	if err := scanner.Err(); err != nil {
		return []DeviceInfo{}, fmt.Errorf("ckb: read device list: %w", err)
	}
	return devices, nil
}

// Device represents a connection to a CKB daemon device. It is not safe for use by multiple goroutines. In order to use multiple programs/goroutines, create separate Devices and ensure that both of them call Device.SetNotificationChannel() with unique indexes.
type Device struct {
	i   DeviceIndex
	cmd *os.File

	notify        *bufio.Reader // notify is a buffered reader of notifyF
	notifyF       *os.File      // notifyF is the notification file
	notifyBuffer  []string      // notifyBuffer is a back buffer of commands received over notify
	notifyIndex   int           // notifyIndex is the index of the notification file
	notifyCaptive bool          // if notifyCaptive is true, notifyoff should be sent to cmd when notifyF is closed
}

// Open opens the device at the given index.
func Open(i DeviceIndex) (*Device, error) {
	if i == 0 {
		return nil, deviceError(i, ErrDeviceNotFound)
	}

	dev := &Device{i: i}
	var err error

	dev.cmd, err = os.OpenFile(fmt.Sprintf(fmtDevCmd, i), os.O_WRONLY, 0)
	if err != nil {
		if errors.Is(err, os.ErrNotExist) {
			return nil, deviceError(i, ErrDeviceNotFound)
		}
		return nil, deviceError(i, err)
	}

	dev.notifyF, err = os.Open(fmt.Sprintf(fmtDevNotify, i, 0))
	if err != nil {
		return nil, deviceError(i, err)
	}
	dev.notify = bufio.NewReader(dev.notifyF)

	dev.sendCmd("active")

	return dev, nil
}

// Close closes the device.
func (dev *Device) Close() error {
	err1 := dev.closeNotifIfNeeded()
	err2 := dev.cmd.Close()
	err := multi(err1, err2)
	if err != nil {
		return deviceError(dev.i, fmt.Errorf("close: %w", err))
	}
	return nil
}

// SwitchMode switches to the given mode.
func (dev *Device) SwitchMode(mode int) error {
	if err := dev.sendModedCmd(mode, "switch"); err != nil {
		return deviceError(dev.i, err)
	}
	return nil
}

// EraseMode clears the state of the given mode.
func (dev *Device) EraseMode(mode int) error {
	if err := dev.sendModedCmd(mode, "erase"); err != nil {
		return deviceError(dev.i, err)
	}
	return nil
}

// NotifyAllKeys enables notifications for all key events.
func (dev *Device) NotifyAllKeys() error {
	err := dev.sendNotifiedCmd(dev.notifyIndex, "notify all")
	if err != nil {
		return deviceError(dev.i, err)
	}
	return nil
}

// UnnotifyAllKeys disables notifications for all key events.
func (dev *Device) UnnotifyAllKeys() error {
	err := dev.sendNotifiedCmd(dev.notifyIndex, "notify all:off")
	if err != nil {
		return deviceError(dev.i, err)
	}
	return nil
}

// NotifyKeys enables notifications for events on the given keys.
func (dev *Device) NotifyKeys(keys []string) error {
	err := dev.sendNotifiedCmd(dev.notifyIndex, "notify "+strings.Join(keys, " "))
	if err != nil {
		return deviceError(dev.i, err)
	}
	return nil
}

// UnnotifyKeys disables notifications for events on the given keys.
func (dev *Device) UnnotifyKeys(keys []string) error {
	err := dev.sendNotifiedCmd(dev.notifyIndex, "notify "+strings.Join(keys, ":off ")+":off")
	if err != nil {
		return deviceError(dev.i, err)
	}
	return nil
}

// Poll waits for and returns an event notification on the device.
func (dev *Device) Poll() (Event, error) {
	for {
		var cmd string
		if len(dev.notifyBuffer) > 0 {
			cmd = dev.notifyBuffer[0]
			dev.notifyBuffer = dev.notifyBuffer[1:]
		} else {
			var err error
			cmd, err = dev.notify.ReadString('\n')
			if err != nil {
				return nil, err
			}
			cmd = cmd[:len(cmd)-1]
		}
		words := strings.Split(cmd, " ")
		switch {
		case len(words) == 2 && words[0] == "key":
			return &KeyEvent{
				Key:     words[1][1:],
				Pressed: words[1][0] == byte('+'),
			}, nil
		default:
			debugf("ckb unhandled event: %s\n", cmd)
		}
	}
}

// SetAllLEDs sets the color of every LED on the device to the given color.
func (dev *Device) SetAllLEDs(mode int, col color.Color) error {
	err := dev.sendModedCmd(mode, "rgb "+colToHex(col))
	if err != nil {
		return deviceError(dev.i, err)
	}
	return nil
}

// SetLED sets the color of the given LED on the device to the given color.
func (dev *Device) SetLED(mode int, key string, col color.Color) error {
	err := dev.sendModedCmd(mode, "rgb "+key+":"+colToHex(col))
	if err != nil {
		return deviceError(dev.i, err)
	}
	return nil
}

// SetLEDs sets the color of the given LEDs on the device to the given color.
func (dev *Device) SetLEDs(mode int, keys []string, col color.Color) error {
	err := dev.sendModedCmd(mode, "rgb "+strings.Join(keys, ",")+":"+colToHex(col))
	if err != nil {
		return deviceError(dev.i, err)
	}
	return nil
}

// SetNotificationChannel sets the channel this device will use to receive notifications from the CKB daemon. By default, devices will use notify0 for all notifications.
// It can be any integer from 0 to 9. If captive is true, this notification channel will be closed when we are done with it; otherwise it will be left open. When in doubt, set captive to true.
//
// See https://github.com/ckb-next/ckb-next/wiki/CKB-Daemon-Manual#notifications
func (dev *Device) SetNotificationChannel(i int, captive bool) error {
	// close existing notif channel
	dev.closeNotifIfNeeded()

	// open new channel
	if err := dev.sendCmd("notifyon " + strconv.Itoa(i)); err != nil {
		return deviceError(dev.i, err)
	}
	dev.notifyIndex = i
	dev.notifyCaptive = captive

	// ckb-daemon can take a little bit to open the new channel
	// so lets keep trying until it exists
	// TODO this is dirty; is there a better way?
	for {
		var err error

		dev.notifyF, err = os.Open(fmt.Sprintf(fmtDevNotify, dev.i, dev.notifyIndex))
		if err != nil {
			// if it doesn't exist, wait 10ms and try again
			if errors.Is(err, os.ErrNotExist) {
				time.Sleep(10 * time.Millisecond)
				continue
			} else {
				return deviceError(dev.i, err)
			}
		}

		break
	}
	// reset buffered reader
	dev.notify.Reset(dev.notifyF)
	return nil
}

// GetMode returns the mode the device is currently set to.
func (dev *Device) GetMode() (int, error) {
	if err := dev.sendNotifiedCmd(dev.notifyIndex, "get :mode"); err != nil {
		return -1, deviceError(dev.i, err)
	}

	// scan until "mode x switch"
	var words []string
	for {
		line, err := dev.notify.ReadString('\n')
		if err != nil {
			return -1, deviceError(dev.i, err)
		}
		line = line[:len(line)-1] // trim delimeter off end
		words = strings.Split(line, " ")

		if len(words) == 3 && words[0] == "mode" && words[2] == "switch" {
			break
		}

		// push unslurped line into a back buffer
		dev.notifyBuffer = append(dev.notifyBuffer, line)
	}

	mode, err := strconv.Atoi(words[1])
	if err != nil {
		return -1, deviceError(dev.i, err)
	}
	return mode, nil
}

// closeNotifIfNeeded ensures dev.notify is closed and sends notifyoff if dev.notifyCaptive is true
func (dev *Device) closeNotifIfNeeded() error {
	if dev.notifyF != nil {
		if err := dev.notifyF.Close(); err != nil {
			return deviceError(dev.i, err)
		}
	}
	if dev.notifyCaptive {
		if err := dev.sendCmd("notifyoff " + strconv.Itoa(dev.notifyIndex)); err != nil {
			return deviceError(dev.i, err)
		}
	}
	return nil
}

func (dev *Device) sendCmd(cmd string) error {
	debugf("ckb%d cmd: %s\n", dev.i, cmd)
	buf := append([]byte(cmd), 0x0a)
	_, err := dev.cmd.Write(buf)
	if err != nil {
		return fmt.Errorf("ckb: write cmd: %w", err)
	}
	return nil
}
func (dev *Device) sendModedCmd(mode int, cmd string) error {
	if mode < 1 || mode > 6 {
		return fmt.Errorf("ckb: mode %d out of range", mode)
	}
	return dev.sendCmd("mode " + strconv.Itoa(mode) + " " + cmd)
}
func (dev *Device) sendNotifiedCmd(notif int, cmd string) error {
	if notif < 0 || notif > 9 {
		return fmt.Errorf("ckb: notification channel %d out of range", notif)
	}
	return dev.sendCmd("@" + strconv.Itoa(notif) + " " + cmd)
}
