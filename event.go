package ckb

type Event interface {
}

type KeyEvent struct {
	Key     string
	Pressed bool
}
